﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.master" AutoEventWireup="true" CodeFile="quiz-detail.aspx.cs" Inherits="quiz_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <asp:TextBox ID="txt_name" runat="server" CssClass="text"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txt_name" WatermarkText="Enter Quiz Name"></asp:TextBoxWatermarkExtender>
        <asp:FileUpload ID="file_image" runat="server" />
        <asp:Button ID="btn_next" runat="server" Text="Next" CssClass="btn-submit" CausesValidation="False" OnClick="btn_next_Click" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </div>
</asp:Content>

