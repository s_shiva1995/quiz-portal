﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="main.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:Label ID="Label1" runat="server" Text="REGISTER" CssClass="label"></asp:Label>
            <asp:TextBox ID="txtname" runat="server" style="width: 200px; margin: auto; display: block; margin-top: 20px" onpaste="return false"></asp:TextBox>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtname" WatermarkText="Enter your Name"></asp:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="namevalidator" runat="server" ErrorMessage="Please enter your name" ControlToValidate="txtname"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtrollno" runat="server" style="width: 200px; margin:auto ; display: block; margin-top: 20px" onpaste="return false"></asp:TextBox>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtrollno" WatermarkText="Enter your Roll number"></asp:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="asas" runat="server" ControlToValidate="txtrollno" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtpass" runat="server" style="width: 200px; margin:auto ; display: block; margin-top: 20px" onpaste="return false"></asp:TextBox>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtpass" WatermarkText="Enter Password"></asp:TextBoxWatermarkExtender>
            <asp:TextBox ID="txtcpass" runat="server" style="width: 200px; margin:auto; display: block; margin-top: 20px" onpaste="return false"></asp:TextBox>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtcpass" WatermarkText="Confirm your Password"></asp:TextBoxWatermarkExtender>
            <asp:CompareValidator ID="lock" runat="server" ControlToCompare="txtcpass" ControlToValidate="txtpass" ErrorMessage="Invalid Password"></asp:CompareValidator>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" TargetControlID="txtrollno" ValidChars="0123456789"></asp:FilteredTextBoxExtender>
            <asp:DropDownList ID="dropbranch" runat="server" style="width: 200px; margin:auto; display: block; margin-top: 20px">
                <asp:ListItem>Computer Science</asp:ListItem>
                <asp:ListItem>Information Technology</asp:ListItem>
                <asp:ListItem>Electronics and Communication</asp:ListItem>
                <asp:ListItem>Electrical and Electronics</asp:ListItem>
                <asp:ListItem>Civil Engineering</asp:ListItem>
                <asp:ListItem>Mechanical Engineering</asp:ListItem>
                <asp:ListItem>Biotechnology</asp:ListItem>
           </asp:DropDownList>
           <asp:DropDownList ID="dropyear" runat="server" style="width: 200px; margin: auto; display: block; margin-top: 20px">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                 </asp:DropDownList>
       </div>
       <div>
          <asp:Button ID="btnregister" runat="server" Text="Register" CssClass="btn-submit" style="position: relative; left: 380px" CausesValidation="true" />
       </div>
    </div>
</asp:Content>

