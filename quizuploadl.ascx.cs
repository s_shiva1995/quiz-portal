﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class quizuploadl : System.Web.UI.UserControl
{
    SqlConnection cn;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString());
        Session["quiz_id"].ToString();
        Session["ques_no"].ToString();
        int lbl_num = Convert.ToInt32(Session["label"]);
        if(!IsPostBack)
        {
            lbl_ques_num.Text = Session["label"].ToString();
            if (Convert.ToInt32(Session["ques_num"]) == 1)
                btnsubmit.Text = "Finish";
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (btnsubmit.Text == "Next")
        {
            datasubmit();
        }
        else
        {
            datasubmit();
            Response.Redirect("~//home_page.aspx");
        }
        Session["label"] = (Convert.ToInt32(Session["label"]) + 1).ToString();
        if(Convert.ToInt32(Session["label"]) == Convert.ToInt32(Session["ques_no"]))
        {
            lbl_ques_num.Text = Session["label"].ToString();
            btnsubmit.Text = "Finish";
        }
        else
        {
            lbl_ques_num.Text = Session["label"].ToString();
        }
    }
    void datasubmit()
    {
        string ques_detail = "insert into ques_detail values ('" + Session["quiz_id"].ToString() + "','" + Convert.ToInt32(lbl_ques_num.Text) + "','" + txt_ques.Text + "','" + txt_ques_a.Text + "','" + txt_ques_b.Text + "','" + txt_ques_c.Text + "','" + txt_ques_d.Text + "','" + dropans.SelectedItem + "')";
        cmd = new SqlCommand(ques_detail, cn);
        cn.Open();
        cmd.ExecuteNonQuery();
        cn.Close();
    }
}