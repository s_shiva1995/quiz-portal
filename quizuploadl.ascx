﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quizuploadl.ascx.cs" Inherits="quizuploadl" %>

<style>
    .textbox{
        font-size: 20px;
        width: 500px;
        height: 100px;
        border-radius: 5px;
        box-shadow: black 5px 5px 5px;
        border: 1px solid #4c1212;
    }
</style>
<div style="width: 600px; height: 400px; border:1px solid black; margin: auto">
    <asp:Label ID="lbl_ques" runat="server" Text="Ques."></asp:Label><asp:Label ID="lbl_ques_num" runat="server" Text="Ques Num"></asp:Label>
    <asp:TextBox ID="txt_ques" runat="server" TextMode="MultiLine" CssClass="textbox"></asp:TextBox>
    <div>
        <div>
            <asp:TextBox ID="txt_ques_a" runat="server" CssClass="text" ></asp:TextBox>
            <asp:TextBox ID="txt_ques_b" runat="server" CssClass="text" ></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="txt_ques_c" runat="server" CssClass="text" ></asp:TextBox>
            <asp:TextBox ID="txt_ques_d" runat="server" CssClass="text" ></asp:TextBox>
        </div>
    </div>
    <div>
        <asp:DropDownList ID="dropans" runat="server" CssClass="drop" >
            <asp:ListItem>A</asp:ListItem>
            <asp:ListItem>B</asp:ListItem>
            <asp:ListItem>C</asp:ListItem>
            <asp:ListItem>D</asp:ListItem>
        </asp:DropDownList>
    </div>
    <asp:Button ID="btnsubmit" runat="server" Text="Next" CausesValidation="False" OnClick="btnsubmit_Click"/>
</div>

