﻿using System;
using System.Configuration;
using System.Data.SqlClient;

public partial class quiz_detail : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString());
        Session["ques_no"].ToString();
    }
    protected void btn_next_Click(object sender, EventArgs e)
    {
        string select = "select * from quiz_detail";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        int i = 1;
        while(rd.Read())
        {
            i++;
        }
        rd.Close();
        string p = (i < 10 ? "0" : "") + i.ToString();
        string quiz_id = Session["ques_no"].ToString() + "quiz" + p;
        string register_quiz = "insert into quiz_detail values ('"+quiz_id+"','"+txt_name.Text+"','"+file_image.FileName+"','"+Session["ques_no"].ToString()+"')";
        cmd = new SqlCommand(register_quiz, cn);
        cmd.ExecuteNonQuery();
        cn.Close();
        Session["quiz_id"] = quiz_id;
        Response.Redirect("~//upload_quiz.aspx");
    }
}