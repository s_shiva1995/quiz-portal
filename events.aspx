﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.master" AutoEventWireup="true" CodeFile="events.aspx.cs" Inherits="events" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="main.css" />
    <script runat="server">
        protected void btn_next_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_teach_pass.Text == "12345")
                {
                    string ques = txt_ques.Text.ToString();
                    Session["ques_no"] = ques;
                    /*int count = 1;
                    Session["counting"] = count.ToString();*/
                    int lbl_num = 1;
                    Session["label"] = lbl_num.ToString();
                    Response.Redirect("~//quiz-detail.aspx");
                }
                else
                {
                    txt_ques.Text = "90";
                }
            }
            catch
            {
                
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="four-block">
        <div class="block-left">
            <div class="block">
                <div style="width: 99.8%; height: 15%; background-color: azure; border: 1px solid black">
                    <h1 style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px; text-align: center;color: black">Teacher's Section</h1>
                </div>
                <div style="width: 70%; height: 84%; margin: auto; border: 1px solid black">
                    <div>
                        <asp:TextBox ID="txt_ques" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt_ques" ValidChars="0123456789"></asp:FilteredTextBoxExtender>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txt_ques" WatermarkText="Number of questions to upload"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:TextBox ID="txt_teach_pass" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txt_teach_pass" WatermarkText="Enter your Password"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:Button ID="btn_next" runat="server" CssClass="btn-submit" Text="Next" style="margin-top: 30px; margin-left: 35%" OnClick="btn_next_Click" CausesValidation="False"/>
                    </div>
                </div>
            </div>
            <div class="block">
                <div style="width: 99.8%; height: 15%; background-color: azure; border: 1px solid black">
                    <h1 style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px; text-align: center;color: black">Result(Teacher)</h1>
                </div>
                <div style="width: 70%; height: 84%; margin: auto; border: 1px solid black">
                    <div>
                        <asp:TextBox ID="txt_branch" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txt_branch" ValidChars="qwertyuiopasdfghjklzxcvbnm"></asp:FilteredTextBoxExtender>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txt_branch" WatermarkText="Enter Branch or Name"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:DropDownList ID="drop_year" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <asp:TextBox ID="txt_teach_pass_result" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 25px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="teac_pass" runat="server" TargetControlID="txt_teach_pass_result" WatermarkText="Enter your Password"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:Button ID="btn_submit" runat="server" CssClass="btn-submit" Text="View" style="margin-top: 20px; margin-left: 35%" OnClick="btn_submit_Click" CausesValidation="False"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-right">
            <div class="block">
                <div style="width: 99.8%; height: 15%; background-color: azure; border: 1px solid black">
                    <h1 style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px; text-align: center;color: black">Student's Section</h1>
                </div>
                <div style="width: 70%; height: 84%; margin: auto; border: 1px solid black">
                    <div>
                        <asp:TextBox ID="txt_roll_no_quiz" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txt_roll_no_quiz" WatermarkText="Enter your Roll Number"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:TextBox ID="txt_pass_quiz" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txt_pass_quiz" WatermarkText="Enter Your Password"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:Button ID="btn_next_quiz" runat="server" CssClass="btn-submit" Text="Next" style="margin-top: 30px; margin-left: 35%" OnClick="btn_next_quiz_Click" CausesValidation="False"/>
                    </div>
                </div>
            </div>
            <div class="block">
                <div style="width: 99.8%; height: 15%; background-color: azure; border: 1px solid black">
                    <h1 style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px; text-align: center;color: black">Results(Student)</h1>
                </div>
                <div style="width: 70%; height: 84%; margin: auto; border: 1px solid black">
                    <div>
                        <asp:TextBox ID="txt_roll_no" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txt_roll_no" WatermarkText="Enter your Roll Number"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:TextBox ID="txt_student_pass" runat="server" style="width: 99%; height: 25%; font-size: 100%; margin-top: 30px"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txt_student_pass" WatermarkText="Enter Your Password"></asp:TextBoxWatermarkExtender>
                    </div>
                    <div>
                        <asp:Button ID="btn_view" runat="server" CssClass="btn-submit" Text="View" style="margin-top: 30px; margin-left: 35%" onClick="btn_view_Click" CausesValidation="False"/>
                    </div>
                </div>
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            </div>
        </div>
    </div>
</asp:Content>

